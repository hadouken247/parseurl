﻿using System.Web.Mvc;

namespace ParseUrlData.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return Content("API is running...");
		}
	}
}