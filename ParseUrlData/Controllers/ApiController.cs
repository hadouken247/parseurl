﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace ParseUrlData.Controllers
{
	public class ApiController : System.Web.Http.ApiController
	{
		[HttpGet]
		public HttpResponseMessage Parse(string data)
		{
			if (Uri.TryCreate(data, UriKind.Absolute, out var webLink))
			{
				try
				{
					using (var client = new WebClient())
					{
						var htmlSource = client.DownloadString(data);
						var urlsCollection = UrlHelper.FindUrlsFromData(htmlSource);
						urlsCollection.ForEach(url => url.Base = webLink.GetLeftPart(UriPartial.Authority));

						using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Test"].ConnectionString))
						{
							var query = "INSERT INTO [dbo].[Urls] (Url, Href) VALUES\n";
							var valuesForQuery = new StringBuilder();
							var lastElement = urlsCollection.Count - 1;

							for (var i = 0; i < lastElement; i++)
							{
								valuesForQuery.AppendLine($"('{urlsCollection[i].Base}', '{urlsCollection[i].Href}')");
								if (i != lastElement - 1)
								{
									valuesForQuery.Append(",");
								}
							}

							query += valuesForQuery;

							using (var command = new SqlCommand {CommandType = CommandType.Text, CommandText = query, Connection = connection})
							{
								connection.Open();
								command.ExecuteNonQuery();
								connection.Close();
								return new HttpResponseMessage(HttpStatusCode.Created);
							}
						}
					}
				}
				catch
				{
					return new HttpResponseMessage(HttpStatusCode.InternalServerError);
				}
			}

			return new HttpResponseMessage(HttpStatusCode.BadRequest);
		}

	}
}