﻿namespace ParseUrlData
{
	public class Url
	{
		public string Base { get; set; }
		public string Href { get; set; }
	}
}