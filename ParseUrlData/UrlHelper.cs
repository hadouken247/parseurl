﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ParseUrlData
{
	public static class UrlHelper
	{
		public static List<Url> FindUrlsFromData(string data)
		{
			var urlCollection = new List<Url>();
			var aTagMatches = Regex.Matches(data, @"(<a.*?>.*?</a>)",
				RegexOptions.Singleline);

			foreach (Match aTag in aTagMatches)
			{
				var url = new Url();
				var aTagInnerHtml = aTag.Groups[1].Value;
				var hrefLinkMatch = Regex.Match(aTagInnerHtml, @"href=\""(.*?)\""",
					RegexOptions.Singleline);
				if (hrefLinkMatch.Success)
				{
					url.Href = hrefLinkMatch.Groups[1].Value;
				}

				urlCollection.Add(url);
			}

			return urlCollection;
		}
	}
}